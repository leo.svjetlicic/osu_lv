import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans


img = Image.imread("./imgs/test_3.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

km = KMeans(n_clusters =3, init='random',n_init=5, random_state =0)
km.fit(img_array)
labels = km.predict(img_array)
img_array_aprox = img_array.copy()

diff_colors = len(np.unique(img_array_aprox,axis=0))

print(diff_colors)

plt.figure(2)
plt.clf()
plt.axis("off")
plt.title("Rekonstruirana slika")
plt.imshow(km.cluster_centers_[labels].reshape(w, h, -1))


sse = {}
for k in range(1, 15):
    kmeans = KMeans(n_clusters=k, max_iter=1000, init='random',
                    n_init=5, random_state=0).fit(img_array)
    sse[k] = kmeans.inertia_

plt.figure(3)
plt.plot(list(sse.keys()), list(sse.values()))
plt.title("J/K ovisnost")
plt.xlabel("K")
plt.ylabel("J")
plt.show()
