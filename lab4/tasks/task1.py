from sklearn import datasets
from sklearn.metrics import mean_absolute_error, mean_absolute_percentage_error, mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import sklearn . linear_model as lm

data = pd.read_csv('data_C02_emission.csv')
# a
numeric_data = data.drop(['Model', 'Make'], axis=1)
X, y = datasets . load_diabetes(return_X_y=True)
input = ['Fuel Consumption City (L/100km)',
         'Fuel Consumption Hwy (L/100km)',
         'Fuel Consumption Comb (L/100km)',
         'Fuel Consumption Comb (mpg)',
         'Engine Size (L)',
         'Cylinders']
output = ['CO2 Emissions (g/km)']

X = data[input].to_numpy()
y = data[output].to_numpy()
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=1)
# b
plt.scatter(X_train[:, 0], y_train, color="blue", label="trening")
plt.scatter(X_test[:, 0], y_test, color="red", label="test")
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()

# c
sc = MinMaxScaler()
X_train_n = sc . fit_transform(X_train)
X_test_n = sc . transform(X_test)
ax1 = plt.subplot(211)
ax1.hist(X_train[:, 0])
ax2 = plt.subplot(212)
ax2.hist(X_train_n[:, 0])
plt.show()

# d
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)

# e
y_test_p = linearModel.predict(X_test_n)
plt.scatter(x=y_test, y=y_test_p, c='g')
plt.show()

# f
MSE = mean_squared_error(y_test, y_test_p, squared=False)
MAE = mean_absolute_error(y_test, y_test_p)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)
print("MSE", MSE)
print("MAE", MAE)
print("MAPE", MAPE)
print("R2", R2)

# g
plt.figure()
plt.scatter(x=X_test_n[:, 0], y=y_test, c='b')
plt.scatter(x=X_test_n[:, 0], y=y_test_p, c='r')
plt.show()
