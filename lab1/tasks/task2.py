category='Nothing'
grade=input("Unesi postotak bodova")
def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False    
while(isfloat(grade)==False and float(grade)>0 and float(grade)<1.0):
    grade=input("Unesi postotak bodova")

grade=float(grade)
if(grade >= 0.9):
    category='A'
elif(grade >= 0.8):
    category='B'
elif(grade >= 0.7):
    category='C'
elif(grade >= 0.6):
    category='D'
else:
    category='F'

print(f'Garde is in the category {category}')
