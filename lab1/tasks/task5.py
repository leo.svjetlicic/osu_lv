
with open('lab1/SMSSpamCollection.txt', 'r', encoding='iso-8859-1') as file:
    sumHam=0
    counterHam=0
    sumSpam=0
    counterSpam=0
    for line in file:
        label, message = line.strip().split('\t')
        words = message.split()
        if label == 'ham':
            sumHam += len(words)
            counterHam += 1
        elif label == 'spam':
            sumSpam += len(words)
            counterSpam += 1
print(f'Ham average is : {sumHam/counterHam:.2f}')
print(f'Spam average is : {sumSpam/counterSpam:.2f}')