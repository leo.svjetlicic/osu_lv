dictionary={}
counter=0
with open('lab1/song.txt', 'r') as file:
    for line in file:
        words = line.strip().split()
        for word in words:
            if word not in dictionary:
                dictionary[word] = 1
            else:
                dictionary[word] += 1

for value in dictionary.values():
    if(value==1):
        counter+=1
print(counter)
file.close()