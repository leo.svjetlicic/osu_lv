from matplotlib.colors import ListedColormap
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn import metrics

from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#a
plt.scatter(x=X_train[:,0],y=X_train[:,1],c=y_train)
plt.scatter(x=X_test[:,0],y=X_test[:,1],c=y_test,marker='x')

#b
LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train,y_train)
y_test_p=LogRegression_model.predict(X_test)
print(y_test_p)

#c
theta_zero=LogRegression_model.intercept_
theta_one,theta_two = LogRegression_model.coef_.T
y=(-theta_zero/theta_one) + (-theta_two/theta_one) * X_train[:,1]
plt.plot(X_train[:,1],y)
plt.show()

#d
cm=metrics.confusion_matrix(y_test,y_test_p)
cmd=metrics.ConfusionMatrixDisplay(confusion_matrix=cm,display_labels=[False,True])

cmd.plot()
plt.show()

#e
boolArray =y_test == y_test_p
cmap=ListedColormap(['black','green'])
plt.scatter(X_test[:,0],X_test[:,1],c=boolArray,cmap=cmap)
plt.show()