import pandas as pd

data = pd.read_csv('data_C02_emission.csv')
#a
print(len(data))
print(data.info())
data.dropna(axis =0)
data.drop_duplicates()
data = data.reset_index(drop=True)
for col in data:
    if(type(col)==object):
        data[col]=data[col].astype('Category')
#b
city=data.sort_values(by='Fuel Consumption City (L/100km)',ascending=False)[['Make','Model','Fuel Consumption City (L/100km)']]
print('highest city consumption: ',city.head(3))
print('\n')
print('lowest city consumption: ',city.tail(3))
#c
data['Engine Size (L)']=data['Engine Size (L)'].astype(float)
engine_size=data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print(len(engine_size))
print(engine_size['CO2 Emissions (g/km)'].mean())
#d
audis=data[(data['Make']=='Audi')]
print('num of audis: ', len(audis))
audis=audis[(audis['Cylinders']==4)]
print('prosjecna emisija', audis['CO2 Emissions (g/km)'].mean())
#e
new_data=data[(data['Cylinders']%2==0) & (data['Cylinders']!=2)]
print(len(new_data))
print('mean emission',new_data['CO2 Emissions (g/km)'].mean())
#f
dizel=data[(data['Fuel Type']=='D')]
benzin=data[(data['Fuel Type']=='X')]
print('dizel: ', dizel['Fuel Consumption City (L/100km)'].mean())
print('benzin: ', benzin['Fuel Consumption City (L/100km)'].mean())
print('dizel median: ', dizel['Fuel Consumption City (L/100km)'].median())
print('benzin median: ', benzin['Fuel Consumption City (L/100km)'].median())
#g
vozila=dizel[(dizel['Cylinders']==4)]
print('vozilo sa max: ', vozila.sort_values(by='Fuel Consumption City (L/100km)').head(1))
#h
rucni=data[(data['Transmission'].str.startswith('M'))]
print(len(rucni))
#i
print(data.corr(numeric_only = True))
print('Komentar: svaka celija prikazuje odnos izmedu 2 varijable\n')