import numpy as np
import matplotlib . pyplot as plt


img = plt.imread('lab2/road.jpg')

if len(img.shape) > 2:
    img = np.mean(img, axis=2)

bright_img = np.clip(img + 50, 0, 255)

cropped_img = img.copy()
cropped_img = np.hsplit(cropped_img, 4)

rotated_img = np.rot90(img)

flipped_img = np.fliplr(img)

plt.figure()
plt.title("original")
plt.imshow(img, cmap='gray')

plt.figure()
plt.title("bright")
plt.imshow(bright_img, cmap='gray')

plt.figure()
plt.title("cropped")
plt.imshow(cropped_img[1], cmap='gray')

plt.figure()
plt.title("flipped")
plt.imshow(flipped_img, cmap='gray')
plt.show()