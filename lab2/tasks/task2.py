import numpy as np
import matplotlib . pyplot as plt

data = np.loadtxt("lab2/data.csv",delimiter=",",skiprows=1)

#a
print(f"number of people is {data.shape[0]}")

spol = data[1:, 0]
visina = data[1:, 1].astype(float)
masa = data[1:, 2]

#b
plt.scatter(visina, masa)
plt.xlabel('Visina (cm)')
plt.ylabel('Masa (kg)')

#c
plt.figure()
plt.scatter(visina[::50], masa[::50])
plt.xlabel('Visina (cm)')
plt.ylabel('Masa (kg)')

#d
min_visina = np.min(visina)
max_visina = np.max(visina)
mean_visina = np.mean(visina)
print('Minimalna visina:', min_visina)
print('Maksimalna visina:', max_visina)
print('Srednja visina:', mean_visina)

#e
m_ind = data[:, 0] == 1
m_data = data[m_ind]
m_min_height = np.min(m_data[:, 1])
m_max_height = np.max(m_data[:, 1])
m_mean_height = np.mean(m_data[:, 1])
print(f"Male\nMin: {m_min_height}\nMax: {m_max_height}\nAvg: {m_mean_height}")
f_ind = data[:, 0] == 0
f_data = data[f_ind]
f_min_height = np.min(f_data[:, 1])
f_max_height = np.max(f_data[:, 1])
f_mean_height = np.mean(f_data[:, 1])
print(f"Female\nMin: {f_min_height}\nMax: {f_max_height}\nAvg: {f_mean_height}")

plt.show()